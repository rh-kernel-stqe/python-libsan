[build-system]
requires = ["hatchling"]
build-backend = "hatchling.build"

[project]
name = "libsan"
description = "Python modules to manage SAN devices"
readme = "README.md"
requires-python = ">=3.6"
dynamic = ["version"]
authors = [
  { name = "Bruno Goncalves", email = "bgoncalv@redhat.com" }]
maintainers = [
  { name = "Bruno Goncalves", email = "bgoncalv@redhat.com" },
  { name = "Martin Hoyer", email = "mhoyer@redhat.com" },
  { name = "Filip Suba", email = "fsuba@redhat.com" }
]
license = "GPL-3.0-or-later"
license-files = { paths = ["LICENSE"]}
classifiers = [
  "Programming Language :: Python :: 3",
  "Programming Language :: Python :: 3 :: Only",
  "Programming Language :: Python :: 3.6",
  "Programming Language :: Python :: 3.7",
  "Programming Language :: Python :: 3.8",
  "Programming Language :: Python :: 3.9",
  "Programming Language :: Python :: 3.10",
  "Programming Language :: Python :: 3.11",
  "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
]

dependencies = [
  "configobj >=5.0.8",
  "netifaces2 >=0.0.13; python_version > '3.6'",
  "netifaces2 >=0.0.13, <=0.0.19; python_version <= '3.6'",
  "requests >=2.27.1",
  "netapp-ontap >=9.11.1.0",
]

[project.urls]
Repository = "https://gitlab.com/rh-kernel-stqe/python-libsan"

[project.scripts]
sancli = "libsan.sancli:main"

[project.optional-dependencies]
ssh = [
  "ssh2-python >= 1.0.0",
]

[tool.ruff]
line-length = 120
src = ["libsan"]
target-version = "py37"
lint.select = [
  "B",    # flake8-bugbear
  "C4",   # flake8-comprehensions
  "E",    # pycodestyle
  "F",    # pyflakes
  "W",    # pycodestyle
  "RUF",  # ruff
  "I",    # isort
  "N",    # pep8-naming
  "UP",   # pyupgrade
  "YTT",  # flake8-2020
  "INP",  # flake8-no-pep420
  "PIE",  # flake8-pie
  "PT",   # flake8-pytest-style
  "RET",  # flake8-return
  "SIM",  # flake8-simplify
  "ARG",  # flake8-unused-arguments
  "PGH",  # pygrep-hooks
  "PLE",  # pylint error
  "TRY",  # tryceratops
]
lint.ignore = [
  "RET504",  # Unnecessary variable assignment before `return`
  "RET505",  # unnecessary `{branch}` after `return` statement
  "SIM105",  # use `contextlib.suppress({exception})`
  "RUF012",  # Mutable class attributes should be annotated with ClassVar
  "E501",    # line lenght check by ruff formatter
]

[tool.pytest.ini_options]
addopts = ["--verbose"]
testpaths = ["tests"]

[tool.coverage.report]
omit = ["_test.py"]

[tool.hatch.version]
path = "libsan/__about__.py"
